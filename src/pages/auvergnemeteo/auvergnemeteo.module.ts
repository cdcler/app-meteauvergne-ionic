import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuvergnemeteoPage } from './auvergnemeteo';

@NgModule({
  declarations: [
    AuvergnemeteoPage,
  ],
  imports: [
    IonicPageModule.forChild(AuvergnemeteoPage),
  ],
})
export class AuvergnemeteoPageModule {}
