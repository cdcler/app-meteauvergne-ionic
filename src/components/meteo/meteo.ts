import {Component, Input, OnInit} from '@angular/core';
import{HttpClient} from "@angular/common/http";

// AJOUTER INPUT
/**
 * Generated class for the MeteoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'meteo',
  templateUrl: 'meteo.html'
})

export class MeteoComponent implements OnInit{

  text: string;
  meteo: object;
@Input() city: string;
          date: string;
          situation: string;
          imagesituation: string;
          tmp: string;
//http est le nom de l'injection de service)
  constructor(private http:HttpClient) {


    console.log('INIT COMPONENT METEO', this);
    this.city = "loading";

    console.log(this.city);
  }


    ngOnInit(): void {
      console.log(this.city);
      console.log("LOADED");
   /*     console.log(this.tmp);*/

      this.http.get ('https://www.prevision-meteo.ch/services/json/clermont-ferrand').subscribe(data => {
          console.log(data);

          this.city = data["city_info"].name;

          this.date = data["current_condition"].date;

          this.situation = data["current_condition"].condition;
          this.imagesituation = data["current_condition"].icon_big;

         this.tmp = data["current_condition"].tmp;
         /*  this.icon = data["current_condition"].icon_big;
          this.lat = data["city_info"].latitude;
          this.lng = data["city_info"].longitude;
*/
      });

/**/
  }
}