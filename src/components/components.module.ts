import { NgModule } from '@angular/core';
import { MeteoComponent } from './meteo/meteo';
import { MeteopuyComponent } from './meteopuy/meteopuy';
// AJOUTER IONIC MODULE      ionic angular

@NgModule({
	declarations: [MeteoComponent,
    MeteopuyComponent],
	imports: [],
	exports: [MeteoComponent,
    MeteopuyComponent]
})
export class ComponentsModule {}
